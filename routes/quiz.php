<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

Route::get('/', 'IndexController@index');
Route::get('/data-tables', 'IndexController@datatables');

Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('master', function () {
    return view('layout.master');
});


// url 	Methods 	handler 	Keterangan
// /game 	GET 	GameController@index 	menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
// /game/create 	GET 	GameController@create 	menampilkan form untuk membuat data pemain film baru
// /game 	POST 	GameController@store 	menyimpan data baru ke tabel Game
// /game/{game_id} 	GET 	GameController@show 	menampilkan detail data pemain film dengan id tertentu
// /game/{game_id}/edit 	GET 	GameController@edit 	menampilkan form untuk edit pemain film dengan id tertentu
// /game/{game_id} 	PUT 	GameController@update 	menyimpan perubahan data pemain film (update) untuk id tertentu
// /game/{game_id} 	DELETE 	GameController@destroy 	menghapus data pemain film dengan id tertentu

Route::get('/game', 'GameController@index');
Route::get('/game/create', 'GameController@create');
Route::post('/game', 'GameController@store');
Route::get('/game/{game_id}', 'GameController@show');
Route::get('/game/{game_id}/edit', 'GameController@edit');
Route::put('/game/{game_id}', 'GameController@update');
Route::delete('/game/{game_id}', 'GameController@destroy');



