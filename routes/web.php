<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

Route::get('/', 'IndexController@index');
Route::get('/data-tables', 'IndexController@datatables');

Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('master', function () {
    return view('layout.master');
});


// url 	Methods 	handler 	Keterangan
// /cast 	GET 	CastController@index 	menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)
// /cast/create 	GET 	CastController@create 	menampilkan form untuk membuat data pemain film baru
// /cast 	POST 	CastController@store 	menyimpan data baru ke tabel Cast
// /cast/{cast_id} 	GET 	CastController@show 	menampilkan detail data pemain film dengan id tertentu
// /cast/{cast_id}/edit 	GET 	CastController@edit 	menampilkan form untuk edit pemain film dengan id tertentu
// /cast/{cast_id} 	PUT 	CastController@update 	menyimpan perubahan data pemain film (update) untuk id tertentu
// /cast/{cast_id} 	DELETE 	CastController@destroy 	menghapus data pemain film dengan id tertentu

Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');



