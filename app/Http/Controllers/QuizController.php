<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;


class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $game = Game::all();
        // dd($game);
        return view('game.index', compact('game'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('game.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $game = new Game;
        $game->nama = $request->nama;
        $game->umur = $request->umur;
        $game->bio = $request->bio;
        $game->save();
        return redirect('/game');
        // dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($game_id)
    {
        $game = Game::where('id', $game_id)->first();
        // $game = Game::find($id);
        return view('game.show', compact('game'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($game_id)
    {
        $game = Game::where('id', $game_id)->first();
        return view('game.edit', compact('game'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $game_id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $game = Game::find($game_id);
        $game->nama = $request->nama;
        $game->umur = $request->umur;
        $game->bio = $request->bio;
        $game->save();

        return redirect('/game');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($game_id)
    {
        $game = Game::find($game_id);
        $game->delete();

        return redirect('/game');
    }
}
