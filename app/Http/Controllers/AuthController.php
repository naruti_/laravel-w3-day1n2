<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register() {
        return view('page.register');
    }
    public function welcome(Request $request) {
        // return 'jeje';
        $first = $request->first;
        $last = $request->last;
        // dd($request->all());
        return view('page.welcome', compact('first', 'last'));
    }
}
