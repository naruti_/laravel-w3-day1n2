@extends('layout.master')
@section('judul')
    List Cast
@endsection
@section('content')
    <a href="/cast/create" class="mb-3 btn btn-secondary">Tambah Cast</a>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>{{ $item->bio }}</td>
                    <td>
                        <form action="/cast/{{ $item->id }}" method="POST">
                            <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>

                </tr>
            @empty
                <h1>Data tidak ada</h1>
            @endforelse


        </tbody>
    </table>
@endsection
