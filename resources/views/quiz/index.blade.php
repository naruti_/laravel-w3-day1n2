@extends('layout.master')
@section('judul')
    List Cast
@endsection
@section('content')
    <a href="/game/create" class="mb-3 btn btn-secondary">Tambah Game</a>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Gameplay</th>
                <th scope="col">Developer</th>
                <th scope="col">Year</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($game as $key => $item)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->gameplay }}</td>
                    <td>{{ $item->developer }}</td>
                    <td>{{ $item->year }}</td>
                    <td>
                        <form action="/game/{{ $item->id }}" method="POST">
                            <a href="/game/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/game/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </form>
                    </td>

                </tr>
            @empty
                <h1>Data tidak ada</h1>
            @endforelse


        </tbody>
    </table>
@endsection
