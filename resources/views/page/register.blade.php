@extends('layout.master')
@section('judul')
   Halaman Form
@endsection
@section('content')
    <h2>Buat Account Baru</h1>
    <h3>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="first">First Name</label><br /><br />
        <input type="text" name="first" /><br /><br />

        <label for="last">Last Name</label><br /><br />
        <input type="text" name="last" /><br /><br />

        <label for="gender">Gender</label><br /><br />
        <input type="radio" name="gender" value="11" />Male <br />
        <input type="radio" name="gender" value="12" />Female<br />

        <br />

        <label for="nationality">Nationality</label><br /><br />
        <select name="nationality" id="">
            <option value="123">Indonesia</option>
            <option value="124">America</option>
            <option value="125">Arab</option>
        </select><br /><br />

        <label for="language">Language Spoken</label><br /><br />
        <input type="checkbox" name="language[]" value="indonesian" />Bahasa Indonesia
        <br />
        <input type="checkbox" name="language[]" value="English" />English <br />
        <input type="checkbox" name="language[]" value="Other" />Other <br />

        <br>
        <label for="bio">Bio</label><br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Submit">
    </form>
@endsection